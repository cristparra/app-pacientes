import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  Image,
  ScrollView,
  AsyncStorage,
  Picker
} from 'react-native';
import Modal from "react-native-modal";
import Agendar from './first/Agendar'
import * as global from '../global/const'
import moment from 'moment'


const citys = [
  {
      label: 'Valparaiso',
  },
  {
      label: 'Viña del mar',
  },
  {
    label: 'Quilpué',
  },
];
export default class Explore extends Component {

  constructor(props) {
    super(props);
    this.state = { 
      selectedButton: "",
      select:"yo" ,
      city:'Valparaiso',
      isModalVisible:false,
      idProfesional:'',
      idSchedule:'',
      getSchedule:'',
      nearProfessional:[],
      name:'',
      tipoAtencion:[],
      selectedAtention:'5bcf7e3b1c2e8f0015bab01a',
      selectedNearProfessional:'',

      fromDate:[],
      toDate:[],
      arrayDate:[]
    };
    this.selectionOnPressAttention = this.selectionOnPressAttention.bind(this);
    this.selectionOnPressPerfil = this.selectionOnPressPerfil.bind(this);
    this.onChangeCity = this.onChangeCity.bind(this);
    this._toggleModal = this._toggleModal.bind(this);
    this.getNearestProfessional=this.getNearestProfessional.bind(this);
    this.getProfessionalColection=this.getProfessionalColection.bind(this);
    this.selectedProfessional=this.selectedProfessional.bind(this);
    this.navigateCalendar=this.navigateCalendar.bind(this);
    this.goBack=this.goBack.bind(this);
    this.onChangeTipoAtencion=this.onChangeTipoAtencion.bind(this);
    this.onChangeNearProfessional=this.onChangeNearProfessional.bind(this);
  }

  _toggleModal = () =>{
    this.setState({ isModalVisible: !this.state.isModalVisible })
  };

  selectionOnPressAttention(userType) {
    this.setState({ selectedButton: userType })
    this.getProfessionalColection()
    
  } 
  selectionOnPressPerfil(perfil){
    this.setState({select:perfil})
  }
  onChangeCity(city){
    this.setState({city:city})
  }
 

  _getToken = async () => {
    try{
        const token = await AsyncStorage.getItem('id_token');
        //alert(token)
        return token;
        //this.props.navigation.navigate('App');
    }
    catch (error) {
        console.log('Error con el token: ' + error.message);
    }
  };

  getProfessionalColection(){
      fetch(global.API_GET_PROFESSIONAL_COLECTION,{
          method:'GET',
      })
      .then((response)=> response.json())
      .then((responseJson)=>{
          console.log("PERSONAL_COLECTION: ",responseJson)
          if(this.state.selectedButton=='enfermeria'){
            var found = responseJson.find(function(element) {
              return element.name == 'Enfermera';
            });
            this.state.idProfesional=found._id
            console.log("id enfermera",found._id)
          }
          else if(this.state.selectedButton=='tOcupacional'){
            var found = responseJson.find(function(element) {
              return element.name == 'Terapeuta Ocupacional';
            });
            this.state.idProfesional=found._id
            console.log("id T. Ocupacional: ",found._id)
          }
          else if(this.state.selectedButton=='nutricionista'){
            var found = responseJson.find(function(element) {
              return element.name == 'Nutricionista';
            });
            this.state.idProfesional=found._id
            console.log("id Nutricionista: ",found._id)
          }
          else if(this.state.selectedButton=='kinesiologo'){
            var found = responseJson.find(function(element) {
              return element.name == 'Kinesiólogo';
            });
            this.state.idProfesional=found._id
            console.log("id Kinesiólogo: ",found._id)
          }
          else if(this.state.selectedButton=='mCirujano'){
            var found = responseJson.find(function(element) {
              return element.name == 'Médico Cirujano';
            });
            this.state.idProfesional=found._id
            console.log("id Médico Cirujano: ",found._id)
          }
          console.log(this.state.idProfesional)
      })
  }

  getScheduleProfessional(){
      const tok=this._getToken().then((value) => {
          
        fetch(global.API_GET_PROFESSIONAL_SCHEDULE+this.state.idSchedule, {
        method:'GET',
            headers: {
                'Content-Type': 'application/json',
                'x-auth-token':value,
            },

        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log("getProfessional Schecule: ",responseJson)
            //obtener los horarios disponibles profesional
            for(let i=0;i<responseJson.availabilities.length;i++){
              this.state.fromDate.push({
                from: responseJson.availabilities[i].scheduleAvailability[0].fromDate
              })
              this.state.toDate.push({
                to:responseJson.availabilities[i].scheduleAvailability[0].toDate
              })
              
            }

            var a = moment(this.state.fromDate[0].from)
            var b = moment(this.state.toDate[0].to)
            //console.log(b.diff(a,'days'))
            console.log("fromDate(moment)",a)
            console.log("toDate",this.state.toDate)
        })
        .catch((error) => {
            console.log(error);
        });

    })
  }
  getNearestProfessional (){
    const tok=this._getToken().then((value) => {
      //kine//5bc023ce43caaf0015b62f7c
        fetch(global.API_GET_NEAREST_PROFESSIONAL+this.state.selectedAtention+"/"+this.state.idProfesional+"/"+this.state.city, {
        method:'GET',
            headers: {
                'Content-Type': 'application/json',
                'x-auth-token':value,
            },

        })
        .then((response) => response.json())
        .then((responseJson) => {
            //console.log("getNearestProfessional: ",responseJson)
            this.setState({
              //idSchedule:responseJson[0]._id,
              getSchedule:responseJson
            })
            console.log('getSchedule',this.state.getSchedule)
            this.setState({
              nearProfessional:[]
            })
            for(let i=0; i<responseJson.length ; i++){
              
                this.state.nearProfessional.push({
                  name:responseJson[i].name+" "+responseJson[i].lastName,
                  study:responseJson[i].professionalTitle.university,
                  id:responseJson[i]._id
                })
                
            }
            console.log("state nearProfesional",this.state.nearProfessional)
            //this.getScheduleProfessional()

        })
        .catch((error) => {
            console.log(error);
        });

    })
  }

  getAtentionType(){
    fetch("https://bhealthapp.herokuapp.com/api/attentionTypes", {
      method:'GET',

      })
      .then((response) => response.json())
      .then((responseJson) => {
          console.log("getAtentionType: ",responseJson)
          
          for(let i=0;i<responseJson.length;i++){
              this.state.tipoAtencion.push({
                label:responseJson[i].name,
                typeId:responseJson[i]._id,
                price:responseJson[i].price
              })
          }
          console.log("state tipoAtencion:",this.state.tipoAtencion)
      })
      .catch((error) => {
          console.log(error);
      });
  }
  componentWillMount(){
    this.getAtentionType()
  }

  onChangeTipoAtencion(selectedAtention){
    this.setState({
      selectedAtention:selectedAtention
    })
  }

  onChangeNearProfessional(selectedNearProfessional){
    this.setState({
      selectedNearProfessional:selectedNearProfessional
    })
  }
 
  selectedProfessional(){
    this.setState({
      idSchedule:this.state.selectedNearProfessional
    })
     
    this.getScheduleProfessional()
    
  }
  
  navigateCalendar=()=>{
    this.props.navigation.navigate("NewScreen",{
      from:this.state.fromDate,
      to:this.state.toDate,
      price:this.state.tipoAtencion[1].price
    })
    this._toggleModal()
  }
  goBack(){
    this.props.navigation.goBack();
  }

  render() {
    return (
      <ImageBackground style={ styles.imgBackground } 
            resizeMode='cover' 
            source={require('./fondo.jpg')}>
        <View style={styles.column}> 
          <Text style={styles.text}> Seleccione profesional:</Text>
          <View style={[styles.row]}>
            <ScrollView horizontal={true} contentContainerStyle={{height: 130 }}>
              <TouchableOpacity
                style={styles.btnimage}
                onPress={() => this.selectionOnPressAttention("kinesiologo")}
              >
                <Image
                  //style={styles.image}
                  style={{  flex: .5, 
                            width: 40, 
                            height: 40, 
                            resizeMode: 'contain',
                            tintColor: this.state.selectedButton === "kinesiologo" ? '#50BAB1' : 'black' 
                        }}
                  //tintColor='blue'
                  source={require('./medicina.png')}
                />
                
                <Text style={{color:'black'}}>kinesiólogo</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.btnimage}
                onPress={() => this.selectionOnPressAttention("enfermeria")}
              >
                <Image
                  style={{  flex: .5, 
                            width: 40, 
                            height: 40, 
                            resizeMode: 'contain',
                            tintColor: this.state.selectedButton === "enfermeria" ? '#50BAB1' : 'black' 
                        }}
                  source={require('./enfermeria.png')}
                />
                <Text style={{color:'black'}}>Enfermería</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.btnimage}
                onPress={() => this.selectionOnPressAttention("tens")}
              >
                <Image
                  style={{  flex: .5, 
                            width: 40, 
                            height: 40, 
                            resizeMode: 'contain',
                            tintColor: this.state.selectedButton === "tens" ? '#50BAB1' : 'black' 
                        }}
                  source={require('./termometro.png')}
                />
                <Text style={{color:'black'}}>Tens</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.btnimage}
                onPress={() => this.selectionOnPressAttention("nutricionista")}
              >
                <Image
                  //style={styles.image}
                  style={{  flex: .5, 
                            width: 40, 
                            height: 40, 
                            resizeMode: 'contain',
                            tintColor: this.state.selectedButton === "nutricionista" ? '#50BAB1' : 'black' 
                        }}
                  //tintColor='blue'
                  source={require('./medicina.png')}
                />
            
                <Text style={{color:'black'}}>Nutricionista</Text>
              </TouchableOpacity>

        <TouchableOpacity
          style={styles.btnimage}
          onPress={() => this.selectionOnPressAttention("tOcupacional")}
        >
            <Image
              //style={styles.image}
              style={{  flex: .5, 
                        width: 40, 
                        height: 40, 
                        resizeMode: 'contain',
                        tintColor: this.state.selectedButton === "tOcupacional" ? '#50BAB1' : 'black' 
                    }}
              //tintColor='blue'
              source={require('./medicina.png')}
            />
            
            <Text style={{color:'black'}}>T. Ocupacional</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.btnimage}
          onPress={() => this.selectionOnPressAttention("mCirujano")}
        >
            <Image
              //style={styles.image}
              style={{  flex: .5, 
                        width: 40, 
                        height: 40, 
                        resizeMode: 'contain',
                        tintColor: this.state.selectedButton === "mCirujano" ? '#50BAB1' : 'black' 
                    }}
              //tintColor='blue'
              source={require('./medicina.png')}
            />
            
            <Text style={{color:'black'}}>Médico General</Text>
        </TouchableOpacity>

        </ScrollView>
      </View>
      
      <Text style={[styles.text]}>Seleccione usuario:</Text>

      <View style={styles.row}>
      <TouchableOpacity
         style={styles.btnimage}
         onPress={() => this.selectionOnPressPerfil("yo")}
       >
          <Image
            style={{  flex: .5, 
                      width: 80, 
                      height: 80, 
                      resizeMode: 'contain',
                      tintColor: this.state.select === "yo" ? '#50BAB1' : 'black' 
                  }}
            source={require('./user.png')}
          />
          <Text style={{color:'black'}}>Yo</Text>

       </TouchableOpacity>

       <TouchableOpacity
         style={styles.btnimage}
         onPress={() => this.selectionOnPressPerfil("otro")}       >
          <Image
            style={{  flex: .5, 
                      width: 80, 
                      height: 80, 
                      resizeMode: 'contain',
                      tintColor: this.state.select === "otro" ? '#50BAB1' : 'black' 
                  }}
            source={require('./user.png')}
          />
          <Text style={{color:'black'}}>Otro</Text>

       </TouchableOpacity>

      </View>
      



      <TouchableOpacity
        style={styles.button}
        onPress={()=>this._toggleModal()}>
        <Text
          style={{fontSize:22,textAlign:'center',color:'black'}}
        >
          Agregar horario
        </Text>
      </TouchableOpacity>

        <Modal
            backdropOpacity={0.9}
            animationIn={'slideInLeft'}
            animationOut={'slideOutRight'}
            isVisible={this.state.isModalVisible}
            backdropColor='grey'
          >
          <Agendar  
            citys={citys}
            city={this.state.city}
            onChangeCity={this.onChangeCity}
            _toggleModal={this._toggleModal}
            getNearestProfessional={this.getNearestProfessional}
            getProfessionalColection={this.getProfessionalColection}
            selectedProfessional={this.selectedProfessional}
            getSchedule={this.state.getSchedule}

            nearProfessional={this.state.nearProfessional}
            onChangeNearProfessional={this.onChangeNearProfessional}
            selectedNearProfessional={this.state.selectedNearProfessional}

            navigateCalendar={this.navigateCalendar}
            goBack={this.goBack}

            onChangeTipoAtencion={this.onChangeTipoAtencion}
            selectedAtention={this.state.selectedAtention}
            tipoAtencion={this.state.tipoAtencion}
          />      
        </Modal>

      
      </View>
      </ImageBackground>
    );
  }
}

/*
  <TouchableOpacity
         style={styles.button}
         onPress={()=>this.props.navigation.navigate('Agendar')}
       >
          <Text style={styles.text}>Confirmar</Text>
       </TouchableOpacity>
 */

const styles = StyleSheet.create({
  column:{
    flex: 1,
    flexDirection:'column',
    marginVertical:100,
    //backgroundColor:'grey'
    backgroundColor:'transparent'
  },
 button:{
    backgroundColor:'#8CD2CC',
    width:220,
    margin:8,
    alignSelf:'center',
    //marginTop:100,
    //marginLeft:80,
    //marginRight:80,
    borderWidth: 2,
    borderRadius:10

  },
  imgBackground: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1 
},
  btnimage:{
    //backgroundColor:'lightblue',
    backgroundColor:'transparent',
    justifyContent:'center',
    alignItems:'center',
    margin:4,
  },
  row: {
    flex: .8,
    flexDirection:'row',
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: 'red',
    backgroundColor:'transparent'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  image: { 
    flex: .5, 
    width: 80, 
    height: 80, 
    resizeMode: 'contain'
  }, 
  text:{
    alignSelf:'center',
    fontSize:25,
    color:'black'
  },
  
});
