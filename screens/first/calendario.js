import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TextInput,
  TouchableOpacity,
  Image,
  TouchableHighlight,
  SectionList,
  ScrollView,
  Alert
} from 'react-native';
import Agendar from './Agendar'
import Modal from "react-native-modal";

import { Calendar,CalendarList} from 'react-native-calendars';
import {LocaleConfig} from 'react-native-calendars';

LocaleConfig.locales['ch'] = {
  monthNames: ['Enero','Febrero','Marso','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
  monthNamesShort: ['En.','Feb.','Mzo','Abr.','My.','Jun.','Jul.','Ag.','Sept.','Oct.','Nov.','Dic.'],
  dayNames: ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'],
  dayNamesShort: ['Dom.','Lun.','Mart.','Miérc.','Juev.','Vier.','Sáb.']
};

LocaleConfig.defaultLocale = 'ch';


arrayMarkedDays=[
  "2018-11-28","2018-11-29","2018-11-30","2018-11-14","2018-12-01","2018-12-02","2018-12-03"
]


const citys = [
  {
      label: 'Valparaiso',
  },
  {
      label: 'Viña del mar',
  },
  {
    label: 'Quilpué',
  },
];


 export default class NewScreen extends React.Component{
  constructor(){
    super();
    hoy = new Date().toJSON().slice(0,10)

    this.state={
      city:'Valparaiso',
      date:'',
      daySelected:'',
      from:'',
      event:{
        from:'',
        to:'',
      },

      cita:[]
    }
    this.onChangeCity=this.onChangeCity.bind(this)
    this.onChangeDate=this.onChangeDate.bind(this)
  }

  onChangeDate(date){
    this.setState({date:date})
  }

  onChangeCity(city){
    this.setState({city:city})
  }

  asdf(from){
    let dates = {};
    this.state.cita.length=0
    arrayMarkedDays.forEach((val) => {
      this.state.cita.push({
        fecha:val
      })
      dates[val] = {marked:true};
    });
    //console.log("state cita:",this.state.cita)
    return dates;
  }

  _toggleModal = () =>
    this.setState({ isModalVisible: !this.state.isModalVisible });
  
    GetSectionListItem=(item)=>{
      Alert.alert("Confirmar atención?",
      "Horario seleccionado: 30-11-2018 "+"\nHora: "+ item +"\nPrecio: $"+this.props.navigation.getParam('price'),
      [
        {text: 'Cancelar', onPress: () => console.log('Cancelar Atención'), style: 'cancel'},
        {text: 'Confirmar', onPress: () => {console.log('Confirmar Atención'),this.props.navigation.goBack(),Alert.alert("","Agendando atención")}},
      ],)
      console.log(item)
    }

  
    render(){
      
      const from = this.props.navigation.getParam('from');
      const to = this.props.navigation.getParam('to');
      const price = this.props.navigation.getParam('price')
      //console.log(price)
      console.log("from calendar",from)
      console.log("to calendar",to)
     //arrayMarkedDays=[ to[0].split("T")[0] ]
/**
 * <Modal
            backdropOpacity={0.9}
            animationIn={'slideInLeft'}
            animationOut={'slideOutRight'}
            isVisible={this.state.isModalVisible}
            backdropColor='#696A69'
          >
            <Agendar
              citys={citys}
              city={this.state.city}
              onChangeCity={this.onChangeCity}
              date={this.state.date}
              onChangeDate={this.onChangeDate}

            />
            <TouchableOpacity onPress={this._toggleModal}
                    >
                        <Text 
                        style={styles.button}
                        >
                            Volver
                        </Text>
            </TouchableOpacity>
          </Modal>
 * 
 */

  /*
    <Text style={{marginTop:20, alignSelf:'center'}}> 
              Disponible:{"\n"}{"   "}Desde:{this.state.event.from}{"\n"}{"   "}Hasta:{this.state.event.to}
            </Text>
   */
        return(
          <View>
            <ScrollView>
            
            <Calendar
            
              // Initially visible month. Default = Date()
              current={hoy}
              // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
              minDate={hoy}
              // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
              maxDate={'2022-12-30'}
              // Handler which gets executed on day press. Default = undefined
              onDayPress={(day) => {

                for(let i=0; i<this.state.cita.length; i++){

                  if(this.state.cita[i].fecha == day.dateString){
                    this.setState({
                      daySelected:day,
                      event: {
                          from: from[0].from.split("T")[1],
                          to:to[0].to.split("T")[1],
                          
                      }
                    })
                  }
                }

                console.log('selected day:<<', day);
                console.log("this state event:",this.state.event);

                }
              }
              // Handler which gets executed on day long press. Default = undefined
              onDayLongPress={(day) => {
                for(let i=0; i<this.state.cita.length; i++){

                  if(this.state.cita[i].fecha == day.dateString){
                    this.setState({
                      daySelected:day,
                      event: {
                          from: from[0].from.split("T")[1],
                          to:to[0].to.split("T")[1],
                          
                      }
                    })
                  }
                  }
                  console.log('selected day:<<', day);
                  console.log("this state event:",this.state.event);
              }}
              // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
              monthFormat={'MMMM yyyy'}
              // Handler which gets executed when visible month changes in calendar. Default = undefined
              onMonthChange={(month) => {console.log('month changed', month)}}
              // Hide month navigation arrows. Default = false
              //hideArrows={true}
              // Replace default arrows with custom ones (direction can be 'left' or 'right')
              //renderArrow={(direction) => (<Arrow />)}
              // Do not show days of other months in month page. Default = false
              hideExtraDays={true}
              // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
              // day from another month that is visible in calendar page. Default = false
              disableMonthChange={true}
              // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
              firstDay={1}
              // Hide day names. Default = false
              hideDayNames={false}
              // Show week numbers to the left. Default = false
              showWeekNumbers={false}
              // Handler which gets executed when press arrow icon left. It receive a callback can go back month
              onPressArrowLeft={substractMonth => substractMonth()}
              // Handler which gets executed when press arrow icon left. It receive a callback can go next month
              onPressArrowRight={addMonth => addMonth()}

              markedDates={{
                hoy:{selected:true,selectedColor:'blue'},
                //'2018-10-26': {selected: false, marked: true, /*selectedColor: 'blue'*/},
                //'2018-11-08': {marked: true},
                //'2018-10-28': {marked: true, dotColor: 'red', activeOpacity: 0},
                //'2018-10-29': {disabled: false, disableTouchEvent: false}
                //[arrayMarkedDays]:{selected:true,selectedColor:'red'}
              }}
            markedDates={this.asdf(from)}
            />       
                        
              <SectionList
                sections={[
                  { title: 'Disponibilidad: 30-11-2018',
                    data: ['09:00', '09:15','09:30','09:45','10:00'
                          ,'10:15','10:30','10:45','11:00',
                          '11:15','11:30','11:45','12:00',
                          '12:15','12:30','12:45'] 
                  }]}
                renderSectionHeader={ 
                  ({section}) => 
                    <Text style={[styles.SectionHeader,{textAlign:'center'}]}> 
                      { section.title } 
                    </Text>
                }
                renderItem={ 
                  ({item}) => 
                  <Text 
                    style={[styles.SectionListItemS,{textAlign:'center'}]} 
                    onPress={()=>this.GetSectionListItem(item)}> 
                      { item } 
                  </Text> }
                keyExtractor={ (item, index) => index }
              />

            

              <TouchableHighlight
                style={[styles.button,{marginTop:20}]}
                onPress={()=>this.props.navigation.goBack()}
              > 
                <Text 
                  style={{alignSelf:'center', fontSize:20,borderRadius:15}}>
                  Volver
                </Text>
              </TouchableHighlight>
            </ScrollView>
          </View>
        );
      }
    
  }

  


  const styles = StyleSheet.create({
    column:{
      flex: 1,
      flexDirection:'column',
      marginVertical:150,
      backgroundColor:'grey'
  
    },
    button:{
      backgroundColor:'#8CD2CC',
      width:220,
      margin:8,
      alignSelf:'center',
      borderWidth: 2,
      borderRadius:10
  
    },
    SectionHeader:{
      backgroundColor : '#8CD2CC',
      fontSize : 20,
      padding: 5,
      color: '#fff',
      fontWeight: 'bold',
      
   },
    SectionListItemS:{
      fontSize : 16,
      padding: 6,
      color: '#000',
      backgroundColor : '#F5F5F5',
      
  }
});
