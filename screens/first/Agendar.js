import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Picker,
  TouchableOpacity
} from 'react-native';
import * as global from '../../global/const'


import DatePicker from 'react-native-datepicker'

export default class Agendar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            text:'Javiera Carrera 252, Valparaiso'

        };
    }


    /*

                    <DatePicker
                        date={this.props.date}
                        style={[styles.input,]}
                        mode='date'
                        placeholder='Fecha'
                        format= 'YYYY-MM-DD'
                        //minDate='2018-10-07'
                        //maxDate='2018-12-13'
                        confirmBtnText='Confirm'
                        cancelBtnText='Cancel'
                        onDateChange={ (date) => this.props.onChangeDate(date) }
                        customStyles={{
                            placeholderText: {
                                color:'black'
                            },
                            dateTouchBody: {borderColor:"black",backgroundColor:'grey'}
                        }}
                    /> */

                    /*<TouchableOpacity 
                    onPress={this.props.getNearestProfessional(this.props.idProfesional,"Valparaiso")}
                    style={{backgroundColor:'#2d8ead',borderWidth: 2,margin:2}}
                >
                      <Text 
                      style={{fontSize:22,color:'black',textAlign:'center'}}
                      >
                          Buscar
                      </Text>
                </TouchableOpacity> */
             
    render(){
        //let idProfesional = this.props.navigation.getParam('idProfesional')
        //let citys = this.props.navigation.getParam('citys')
        return(
            <View style={styles.container}>
            
                <Text style={styles.text}>Agendar hora</Text>

                <TextInput
                    
                    placeholder="Agregar dirección de atención"
                    onChangeText={(text) => this.setState({text})}
                    value={this.state.text}
                />

                <Picker
                    prompt='Seleccionar ciudad'
                    mode='dialog'
                    selectedValue={this.props.city}
                    onValueChange={itemValue => this.props.onChangeCity(itemValue)}>
                    {this.props.citys.map((i, index) => (
                    <Picker.Item key={index} color={'black'} label={i.label} value={i.label} 
                        
                    />
                    ))}
                        
                </Picker>                
                
                <Picker
                    prompt='Seleccionar tipo de atención'
                    mode='dialog'
                    selectedValue={this.props.selectedAtention}
                    onValueChange={itemValue => {this.props.onChangeTipoAtencion(itemValue),this.props.getNearestProfessional()}}>
                    {this.props.tipoAtencion.map((i, index) => (
                    <Picker.Item key={index} color={'black'} label={i.label} value={i.typeId} 
                        
                    />
                    ))}
                        
                </Picker>   

                <Picker
                    prompt='Seleccionar profesional'
                    mode='dialog'
                    selectedValue={this.props.selectedNearProfessional}
                    onValueChange={itemValue => this.props.onChangeNearProfessional(itemValue)}>
                    {this.props.nearProfessional.map((i, index) => (
                    <Picker.Item key={index} color={'black'} label={i.name +"\n"+ i.study} value={i.id} 
                        
                    />
                    ))}
                        
                </Picker>



                <View>

                    <TouchableOpacity onPress={()=>{this.props.selectedProfessional(),this.props.navigateCalendar()}}
                    style={styles.button}
                    >
                        <Text 
                        style={{fontSize:22,color:'black',textAlign:'center'}}
                        >
                            Continuar
                        </Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={this.props._toggleModal}
                    style={styles.button}
                    >
                        <Text 
                        style={{fontSize:22,color:'black',textAlign:'center'}}
                        >
                            Volver
                        </Text>
                    </TouchableOpacity>

                </View>
            </View>
            
        )    
    }
}
const styles =StyleSheet.create({
    container:{
        marginTop:40,
    },
    modalTop:{
        justifyContent:'flex-start',
        marginTop:50
    },
    input:{
        width:350,
        alignSelf:'center',
        marginBottom:5,
        borderColor:'black',
        borderWidth:1,
        borderRadius:3,
    },
    text:{
        textAlign:'center',
        fontSize:25,
        color:'black',
        //backgroundColor:'#2d8ead'
    },
    button:{
        backgroundColor:'#8CD2CC',
        width:220,
        margin:8,
        alignSelf:'center',
        borderWidth: 2,
        borderRadius:10
    
      },
})