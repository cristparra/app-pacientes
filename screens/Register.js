import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TextInput,
  TouchableOpacity,
  ScrollView,
  TouchableHighlight,
  Image,
} from 'react-native';
import * as global from '../global/const'

import moment from 'moment'
import t from 'tcomb-form-native'

//const t = require('tcomb-form-native');

const Form = t.form.Form


colors = [
    {value: 'green', text: 'Green'},
    {value: 'blue', text: 'Blue'},
    {value: 'other', text: 'Other'}
  ];

const UserRegister = t.struct({
    name:t.String,
    lastname:t.String,
    email: t.String,
    rut:t.String,
    phone:t.String,
    address: t.String,
    dateOfBirth: t.Date,
    password:  t.String,
    
})

const options = {
  fields: {
    name:{
        label:"Nombre"
    },
    lastname:{
        label:"Apellidos"
    },
    email: {
        label:"Correo",
        autoCapitalize: 'none',
        autoCorrect: false
    },
    rut:{
        label:"Rut",
        placeholder:"12345678-9"
    },
    phone:{
        label:"Teléfono",
        placeholder:"569-123-45678"
    },
    address:{
        label:"Dirección"
    },
    
    password: {
        label:"Contraseña",
        autoCapitalize: 'none',
        password: true,
        autoCorrect: false,
        secureTextEntry: true,
    },
    dateOfBirth:{
        label:'Fecha de nacimiento',
        mode:'date',
        config: {
            format: (date) => moment(date).format('YYYY-MM-DD'),
          },
        
    },
    
    

  },
  //stylesheet: formStyles,
  
}

var prof=[];
var profes={}

export default class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: {
                name:"",
                lastname:"",
                email: '',
                rut:'',
                phone:"569-751-83778",
                address:"",
                password: '',
                profession:[]
            }
        };
      }



    _onChange = (value) => {
        this.setState({
        value
        })
    }

    _handleAdd = () => {
        const value = this.refs.form.getValue();
        // If the form is valid...
        if (value) {
        const data = {
            name:value.name,
            lastname:value.lastname,
            email: value.email,
            rut:value.rut,
            phone:value.phone,
            address:value.address,
            dateOfBirth:value.dateOfBirth,
            password: value.password
        }
        // Serialize and post the data
        const json = JSON.stringify(data);
        fetch(global.API_REGISTER, {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json'
            },
            body: JSON.stringify({
                "name": value.name,
                "rut": value.rut,
                "genre":1,
                "city":"Valparaiso",
                "lastName": value.lastname,
                "email": value.email,
                "phone": value.phone,
                "preferedAddress": value.address,
                "password": value.password,
                "dateOfBirth": value.dateOfBirth,
                "personalPhoto": {
                    "url": "url"
                },
                "userType": 1,
            })
        })
        .then((response) => response.text())
        .then((responseText) => {
            console.log(responseText)
            if (responseText.error) {
                console.log(responseText);
            } else {
                
                alert(responseText +"      ")

            }
        })
        .catch((error) => {
            console.log(error);
            //this.onLoginFail();
            alert('There was an error logging in.');
        })
        .done()
        } else {
        // Form validation error
        alert('Please fix the errors listed and try again.')
        }
    }

    componentWillMount(){
        //this.getProfession()
    }


    getProfession (){
        
                fetch(global.API_GET_PROFESSION, {
                  method:'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        Accept: 'application/json'
                    },

                })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log("responseJson: ",responseJson)
                   /* this.setState({
                        profession:responseJson
                    })*/
                    profes=responseJson
                    for(let i  = 0; i<responseJson.length; i++){
                   /*     this.setState({
                            profession:[...this.state.profession,responseJson[i].name]
                        })*/
                        prof.push(responseJson[i].name)
                        console.log(responseJson[i].name);                     
                    
                    }
                    
                  
                    console.log("professions: ",prof)

                })
                .catch((error) => {
                    console.log(error);
                });

    }

    Back=()=>{
        this.props.navigation.goBack()    
    }
    render() {
        
      return (
        <ScrollView style={styles.container}>

                <View style={styles.container2}>

                    <ImageBackground style={ styles.imgBackground } 
                    resizeMode='cover' 
                    source={require('../assets/fondo2.png')}>
                        <Form 
                        ref='form'
                        options={options}
                        type={UserRegister}
                        value={this.state.value}
                        onChange={this._onChange}
                        
                        />

                        <TouchableHighlight onPress={this._handleAdd}>
                        <Text style={[styles.button, styles.greenButton]}>Entrar</Text>
                        </TouchableHighlight>

                        <TouchableHighlight onPress={this.Back}>
                        <Text style={styles.transparentButton}>Volver</Text>
                        </TouchableHighlight>
                    </ImageBackground>

                </View>
        </ScrollView>
      );
    }
  }



  const styles = StyleSheet.create({
    container: {
      //padding: 20,
      flex: 1,
      flexDirection: 'column',
      backgroundColor: '#ffffff'
    },
    container2: {
        padding: 20,
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#ffffff'
      },
    imgBackground: {
      width: '100%',
      height: '100%',
  },
  logo:{
      flex:.1,
      justifyContent: 'center',
      alignItems: 'center',
      //paddingLeft:50,
      //paddingRight:50,
      width:220,
      height:220,
    },
    button: {
      borderRadius: 4,
      padding: 20,
      textAlign: 'center',
      marginBottom: 20,
      //fontSize: 20,
      color: 'black'
    },
    greenButton: {
      backgroundColor: '#446E6E'
    },
    transparentButton:{
      padding:8,
      marginBottom:20,
      color:'black',
      backgroundColor:'#446E6E',
      textAlign:'center',
      alignSelf:'center',
      width:180,
      //fontSize: 20,
      },
    centering: {
      alignItems: 'center',
      justifyContent: 'center'
    },
    
  })
  