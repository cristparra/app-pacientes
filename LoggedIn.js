import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';

import{createBottomTabNavigator}from 'react-navigation'

import Explore from './screens/Explore'
import Inbox from './screens/Inbox'
import Saved from './screens/Saved'
import User from './screens/User'




export default createBottomTabNavigator({
  Explore:{
    screen:Explore,
    navigationOptions:{
      tabBarLabel: '',
      title:"",
      tabBarIcon: ({tintColor}) =>(
       <Image source={require('./assets/ambulance.png')} style={{height:28,width:28,tintColor:tintColor}} />
      )
    }
  },
  Saved:{
    screen:Saved,
    navigationOptions:{
      tabBarLabel: '',
      title:"",
      tabBarIcon: ({tintColor}) =>(
        <Image source={require('./assets/doc.png')} style={{height:28,width:28,tintColor:tintColor}} />
      )
    }
  },
  User:{
    screen:User,
    navigationOptions:{
      tabBarLabel: '',
      title:"",
      tabBarIcon: ({tintColor}) =>(
        <Image source={require('./assets/user.png')} style={{height:28,width:28,tintColor:tintColor}} />
      )
    }
  },
  Inbox:{
    screen:Inbox,
    navigationOptions:{
      tabBarLabel: '',
      title:"",
      tabBarIcon: ({tintColor}) =>(
        <Image source={require('./assets/config.png')} style={{height:28,width:28,tintColor:tintColor}} />
      )
    }
  }
},
{
  tabBarOptions:{
    activeTintColor:'#446E6E',
    inactiveTintColor:'white',
    style:{
      backgroundColor:'#50BAB1',
      borderTopWidth:0,
      shadowOffset:{width:5,height:3},
      shadowColor:'black',
      shadowOpacity:0.5,
      elevation:5
    
    }
  }
}
)
