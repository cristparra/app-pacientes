# Aplicación bHelath para pacientes

## Instalar react native
Para correr el proyecto es necesario tener instalado react-native, que a su vez debe tener instalado Node, la interfaz de línea de comandos de React Native, Python2, un [JDK](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) y Android Studio.

Para instalar react native cli ejecutar el siguiente comando:

```
npm install -g react-native-cli
```
##Proyecto
Una vez hechos los pasos anteriores, para ejecutar el proyecyo debe instalar las dependencias usadas por lo que debe ejecutar el siguiente comando:

```
npm install
```
Para compilar la aplicación y poder verla se debe tener conectado un celular con sistema operativo android mediante un usb al computador, obien tener corriendo un emulador de celular. Posteriormente se debe abrir una consola y situarse en el directorio del proyecto y ejecutar el siguiente comando:

```
react-native run-android
```
El proyecto se compilará y se ejecutará en su dispositivo

##Aplicación 
A continunación se muestran gif del funcionamiento de la aplicación:

Login:
![Login](https://bitbucket.org/cristparra/app-pacientes/raw/138068f6bcde89946b2be29272f609372ae20325/capturas/login%20pacientes.gif)

Registro:
![Registro](https://bitbucket.org/cristparra/app-pacientes/raw/138068f6bcde89946b2be29272f609372ae20325/capturas/registro%20paciente.gif)

Agendado:
![Agendado](https://bitbucket.org/cristparra/app-pacientes/raw/138068f6bcde89946b2be29272f609372ae20325/capturas/agendado%20paciente2.0.gif)

Agenda:
![Agenda](https://bitbucket.org/cristparra/app-pacientes/raw/138068f6bcde89946b2be29272f609372ae20325/capturas/agenda%20con%20logout%20paciente.gif)




