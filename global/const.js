
export const  API = 'https://bhealthapp.herokuapp.com'
export const API_LOGIN = API + '/api/auth/patient'
export const API_REGISTER = API + '/api/registerPatient'


export const API_GET_PROFESSION = API + '/api/professions'

export const API_GET_NEAREST_PROFESSIONAL = API + '/api/nearest/'
export const API_GET_PROFESSIONAL_COLECTION = API + '/api/professions'
export const API_GET_PROFESSIONAL_SCHEDULE = API +'/api/returns/schedules/'