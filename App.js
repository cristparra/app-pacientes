/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  ActivityIndicator,
  StatusBar,
  AsyncStorage
} from 'react-native';

import { createStackNavigator, createSwitchNavigator } from 'react-navigation';

import Explore from './screens/Explore'
import Inbox from './screens/Inbox'
import Saved from './screens/Saved'
import User from './screens/User'

import LoggedIn from './LoggedIn'
import LoginView2 from './screens/LoginView2'
import LoginView from './screens/LoginView'
import Register from './screens/Register'
import Register2 from './screens/Register2'

import NewScreen from './screens/first/calendario'
import Agendar from './screens/first/Agendar'

 



class AuthLoadingScreen extends React.Component {
  constructor() {
    super();
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('id_token');

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    this.props.navigation.navigate(userToken ? 'App' : 'Auth');
  };

  // Render any loading content that you like here
  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}


const AppStack = createStackNavigator(
  { 
    Home: LoggedIn, 
    Explore: Explore,
    Agendar:Agendar,
    NewScreen: NewScreen, 
    
  },
  {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    }
  }
);
const AuthStack = createStackNavigator(
  { 
    LoginView: LoginView2,
    Register:Register,
  },
  {
    initialRouteName:'LoginView',
    headerMode: 'none',
  } 
);

export default createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  }
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
